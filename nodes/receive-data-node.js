'use strict';

module.exports = function (RED) {
  // const fs = require('fs');
  var myNode = void 0;
  var AD_FACTOR = 25;
  var WAVEFORM_PATTEN = 'Waveform;';
  var BLOCK_DATA_PATTEN = '#510000';
  var WAVEFORM_PATTEN_LENGTH = WAVEFORM_PATTEN.length;
  var BLOCK_DATA_PATTEN_LENGTH = BLOCK_DATA_PATTEN.length;

  // const PD_INFO_HEADER = {
  //   magic: 'MagicCode-ZYC-PDM',
  //   date: 'Date',
  //   intpDistance: 'Start Time',
  //   trigAddr: 'Sample Period',
  //   trigLevel: 'Voltage Scale',
  //   source: 'Voltage High',
  //   vUnit: 'Channel Number',
  // };
  var Red = void 0;

  var shiftBufferData = function shiftBufferData(buf, offset, length) {
    var tbuf = Buffer.allocUnsafe(length);
    var len = 0;

    buf.copy(tbuf, 0, offset, offset + length);
    buf.fill(0);
    len = tbuf.copy(buf, 0);
    tbuf = null;

    return len;
  };

  var getHeaderInfo = function getHeaderInfo(patten, src) {
    var strArray = src.split(';');
    var matchString = null;
    // let matchArray;

    for (var i = 0, len = strArray.length; i < len; i += 1) {
      if (strArray[i].search(patten) >= 0) {
        matchString = strArray[i];
        break;
      }
    }

    if (matchString) {
      if (patten === 'Date') {
        return matchString.slice(5, -1);
      }
      return matchString.split(':')[1];
    }
    return null;
  };

  var savePDinfo = function savePDinfo(filepath, pdInfo) {
    // pdInfo.header
    // let fd;
    // const jstring = '';
    var pdObj = { header: {}, rawdata: [] };

    try {
      pdObj.header.date = getHeaderInfo('Date', pdInfo.pdHead);
      pdObj.header.chNum = Number(getHeaderInfo('Channel Number', pdInfo.pdHead));
      pdObj.header.startTime = Number(getHeaderInfo('Start Time', pdInfo.pdHead));
      pdObj.header.offset = Number(getHeaderInfo('Vlow Offset', pdInfo.pdHead));
      pdObj.header.sampleRate = Number(getHeaderInfo('Sample Period', pdInfo.pdHead));
      pdObj.header.vScale = Number(getHeaderInfo('Voltage Scale', pdInfo.pdHead)) / AD_FACTOR;
      pdObj.header.vHigh = Number(getHeaderInfo('Voltage High', pdInfo.pdHead)) * pdObj.header.vScale;

      // pdObj.rawdata = pdInfo.recData;
      for (var i = 0, len = pdInfo.recCount; i < len;) {
        // pdObj.rawdata.push(pdInfo.recData.readInt8(i));
        pdObj.rawdata.push(pdInfo.recData.readInt16LE(i));
        i += 2;
      }
      pdInfo.pdm.push(pdObj);
      // jstring = JSON.stringify(pdObj);
      // fd = fs.openSync(`./waveform/test-${pdObj.header.date}-${pdObj.header.startTime}`, 'w+');
      // fs.writeSync(fd, jstring, 0, jstring.length);
      // myNode.status({fill:"green",shape:"dot",text:"save ok"});
    } catch (e) {
      console.log(e);
      myNode.status({ fill: 'red', shape: 'ring', text: 'save failed' });
      // The main node definition - most things happen in here
    }
  };

  var savePDinfo_forMotor = function savePDinfo_forMotor(filepath, pdInfo) {
    var pdObj = { header: {}, rawdata: [] };

    try {
      pdObj.header.date = getHeaderInfo('Date', pdInfo.pdHead);
      pdObj.header.chNum = Number(getHeaderInfo('Channel Number', pdInfo.pdHead));
      pdObj.header.sampleRate = Number(getHeaderInfo('Sample Period', pdInfo.pdHead));
      pdObj.header.vScale = Number(getHeaderInfo('Voltage Scale', pdInfo.pdHead)) / AD_FACTOR;
      pdObj.header.vHigh = Number(getHeaderInfo('Voltage High', pdInfo.pdHead)) * pdObj.header.vScale;
      pdObj.header.vLow = Number(getHeaderInfo('Voltage Low', pdInfo.pdHead)) * pdObj.header.vScale;
      pdObj.header.vHigh_phase = Number(getHeaderInfo('vHigh Phase Angle', pdInfo.pdHead));
      pdObj.header.vLow_phase = Number(getHeaderInfo('vLow Phase Angle', pdInfo.pdHead));

      pdInfo.pdm.push(pdObj);
    } catch (e) {
      console.log(e);
      myNode.status({ fill: 'red', shape: 'ring', text: 'save failed' });
    }
  };

  var pdInformationDecode = function pdInformationDecode(orgInfo, data) {
    var pdInfo = orgInfo;
    var dataLength = void 0;
    var leftLength = data.length;
    var state = false;
    // Red.log.debug(`data length = ${data.length}`);
    // Red.log.debug(data);
    while (leftLength) {
      if (pdInfo.pdHeadReady === false) {
        var wfOffset = void 0;
        var recStr = '';

        // Waveform Data
        // Red.log.debug('check header');
        // Red.log.debug('pdInfo.tmplength = ' + pdInfo.tmplength);

        dataLength = data.copy(pdInfo.tmpbuffer, pdInfo.tmplength, data.length - leftLength, data.length);
        // Red.log.debug('1: coped dataLength = ' + dataLength);

        leftLength -= dataLength;
        pdInfo.tmplength += dataLength;
        recStr = pdInfo.tmpbuffer.toString();
        wfOffset = recStr.search(WAVEFORM_PATTEN);
        if (wfOffset !== -1) {
          var tstr = void 0;

          pdInfo.pdHead = recStr.slice(0, wfOffset);

          // Red.log.debug('pdHead: ' + pdInfo.pdHead);
          // Red.log.debug('wfOffset = ' + wfOffset);
          // Red.log.debug('pdInfo.tmplength = ' + pdInfo.tmplength);
          // Red.log.debug('-------------------- check pd header ----------------');
          pdInfo.pdHeadReady = true;
          if (pdInfo.tmplength >= wfOffset + WAVEFORM_PATTEN_LENGTH + BLOCK_DATA_PATTEN_LENGTH) {
            tstr = recStr.slice(wfOffset + WAVEFORM_PATTEN_LENGTH);
            // find #510000 patten
            if (tstr[0] === '#') {
              var num = Number(tstr[1]);
              var numstr = tstr.slice(2, Number(tstr[1]) + 2);

              if (Number.isNaN(tstr[1])) {
                // if(cp){
                //     cp(err);
                // }
                Red.log.debug('waveform header format not correct');
                myNode.status({ fill: 'red', shape: 'ring', text: 'header not recongnize' });
                return false;
              }
              pdInfo.headerlen = num + 2;
              Red.log.debug('numstr=' + numstr);
              // block data length + NL
              pdInfo.dataCount = parseInt(numstr, 10);

              Red.log.debug('pdInfo.dataCount = ' + pdInfo.dataCount);

              delete pdInfo.recData;
              pdInfo.recData = Buffer.allocUnsafe(pdInfo.dataCount);

              wfOffset = wfOffset + WAVEFORM_PATTEN_LENGTH + pdInfo.headerlen;
              // check if more than one pd information
              if (pdInfo.tmplength >= pdInfo.dataCount + wfOffset) {
                pdInfo.pdHeadReady = false;
                dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, wfOffset, wfOffset + pdInfo.dataCount);
                if (dataLength === 0) {
                  Red.log.debug('2: copy faile = ' + dataLength);
                  // pdInfo.recData = new Buffer.allocUnsafe(pdInfo.dataCount);
                  dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, wfOffset, wfOffset + pdInfo.dataCount);
                }
                Red.log.debug('2: coped dataLength = ' + dataLength);

                // Red.log.debug(`2: pdInfo.recCount = ${pdInfo.recCount}`);
                // Red.log.debug(`2: wfOffset = ${wfOffset}`);
                // Red.log.debug(`2: wfOffset + pdInfo.dataCount = ${wfOffset + pdInfo.dataCount}`);


                pdInfo.recCount = dataLength;
                Red.log.debug('savePDinfo 22');
                savePDinfo(pdInfo.filepath, pdInfo);
                pdInfo.recCount = 0;
                state = true;
                if (pdInfo.tmplength > pdInfo.dataCount + wfOffset) {
                  Red.log.debug('-------------------- continue ----------------');
                  wfOffset += pdInfo.dataCount;
                  pdInfo.tmplength = shiftBufferData(pdInfo.tmpbuffer, wfOffset, pdInfo.tmplength - wfOffset);
                  pdInfo.dataCount = 0;
                  // continue;
                } else {
                  // save pd data to file and return;
                  pdInfo.tmpbuffer.fill(0);
                  pdInfo.tmplength = 0;
                  pdInfo.dataCount = 0;
                  // return;
                }
              } else {
                dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, wfOffset, pdInfo.tmplength);
                Red.log.debug('3: coped dataLength = ' + dataLength);
                pdInfo.recCount = dataLength;
                pdInfo.tmpbuffer.fill(0);
                pdInfo.tmplength = 0;
                // return;
              }
            } else {
              Red.log.debug('!! err : cant find # tstr[0] = ' + tstr[0]);
              myNode.status({ fill: 'red', shape: 'ring', text: 'header not recongnize' });
              pdInfo.tmplength = 0;
              pdInfo.header = '';
              pdInfo.pdHeadReady = false;
              return false;
            }
          } else {
            // header check done , but '#510000' pattern not complete reciver.
            wfOffset += WAVEFORM_PATTEN_LENGTH;
            try {
              pdInfo.tmplength = shiftBufferData(pdInfo.tmpbuffer, wfOffset, pdInfo.tmplength - wfOffset);
            } catch (e) {
              console.log(e);
            }
            // return;
          }
        } else {
          pdInfo.tmplength = 0;

          wfOffset = recStr.search("HeadEnd");
          // console.log(`wfOffset = ${wfOffset}`);

          if (wfOffset !== -1) {
            // Red.log.debug('pd type: HFCT_MOTOR');
            pdInfo.pdHead = recStr.slice(0, wfOffset);
            state = true;
            // console.log('pdHead: ' + pdInfo.pdHead);
            // console.log('wfOffset = ' + wfOffset);
            savePDinfo_forMotor(pdInfo.filepath, pdInfo);
            pdInfo.recCount = 0;
            pdInfo.tmpbuffer.fill(0);
          } else if (recStr.search('No PD data') !== -1) {
            // Red.log.debug('No PD data');
            state = true;
            // return true;
          } 
        }
      } else if (pdInfo.dataCount === 0) {
        dataLength = data.copy(pdInfo.tmpbuffer, pdInfo.tmplength, data.length - leftLength, data.length);
        leftLength -= dataLength;

        // check waveform header '#xxxxx'
        pdInfo.tmplength += dataLength;

        pdInfo.header = pdInfo.tmpbuffer.slice(0, 20).toString();
        // Red.log.debug('data length = ' + data.length);
        // Red.log.debug('pdInfo.tmplength = ' + pdInfo.tmplength);
        // Red.log.debug('pdInfo.tmpbuffer = '  + pdInfo.tmpbuffer);
        // Red.log.debug('pdInfo.header = '  + pdInfo.header);
        // Red.log.debug('-------------------- check waveform header ----------------');

        if (pdInfo.header[0] === '#') {
          var _num = Number(pdInfo.header[1]);
          var _numstr = pdInfo.header.slice(2, Number(pdInfo.header[1]) + 2);

          pdInfo.headerlen = _num + 2;
          // Red.log.debug('numstr=' + _numstr);
          // block data length + NL
          pdInfo.dataCount = parseInt(_numstr, 10);

          // Red.log.debug('pdInfo.dataCount = ' + pdInfo.dataCount);

          delete pdInfo.recData;
          pdInfo.recData = Buffer.allocUnsafe(pdInfo.dataCount);

          if (pdInfo.tmplength >= pdInfo.dataCount + pdInfo.headerlen) {
            pdInfo.pdHeadReady = false;

            dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.headerlen + pdInfo.dataCount);

            pdInfo.recCount = dataLength;
            // ////////////////
            if (dataLength === 0) {
              // Red.log.debug('33: copy faile = ' + dataLength);
              // pdInfo.recData = Buffer.allocUnsafe(pdInfo.dataCount);
              dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.headerlen + pdInfo.dataCount);
            }
            pdInfo.recCount = dataLength;

            // Red.log.debug('33: copy dataLength = ' + dataLength);
            // Red.log.debug(`33: pdInfo.recCount = ${pdInfo.recCount}`);
            // Red.log.debug(`33: pdInfo.headerlen = ${pdInfo.headerlen}`);
            // Red.log.debug(`33: pdInfo.headerlen +
            // pdInfo.dataCount = ${pdInfo.headerlen + pdInfo.dataCount}`);


            savePDinfo(pdInfo.filepath, pdInfo);
            pdInfo.recCount = 0;
            state = true;
            if (pdInfo.tmplength > pdInfo.dataCount + pdInfo.headerlen) {
              // Red.log.debug('-------------------- continue ----------------');
              pdInfo.tmplength = shiftBufferData(pdInfo.tmpbuffer, pdInfo.headerlen, pdInfo.tmplength - pdInfo.headerlen);
              pdInfo.dataCount = 0;
              // continue;
            } else {
              pdInfo.tmpbuffer.fill(0);
              pdInfo.tmplength = 0;
              pdInfo.dataCount = 0;
              // return;
            }
          } else {
            dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.tmplength);
            // Red.log.debug('4: coped dataLength = ' + dataLength);
            pdInfo.recCount = dataLength;
            pdInfo.tmpbuffer.fill(0);
            pdInfo.tmplength = 0;
            // return;
          }
        } else {
          pdInfo.tmplength = 0;
          pdInfo.header = '';
          myNode.status({ fill: 'red', shape: 'ring', text: 'header not recongnize' });
          pdInfo.pdHeadReady = false;
          return false;
        }
      } else {
        // Red.log.debug('-------------------- check rawdata end ----------------');
        // Red.log.debug('pdInfo.recCount=' + pdInfo.recCount + ',data.length= ' + data.length);
        dataLength = data.copy(pdInfo.recData, pdInfo.recCount, data.length - leftLength, data.length);
        leftLength -= dataLength;
        // Red.log.debug('6: coped dataLength = ' + dataLength);

        pdInfo.recCount += dataLength;

        if (pdInfo.recCount === pdInfo.dataCount) {
          // Red.log.debug('savePDinfo 11');
          savePDinfo(pdInfo.filepath, pdInfo);
          state = true;
          pdInfo.pdHeadReady = false;
          pdInfo.recCount = 0;
          pdInfo.dataCount = 0;
          pdInfo.header = '';
          // continue;
        }
      }
    }

    return state;
  };

  var screenDataDecode = function screenDataDecode(orgInfo, data) {
    var pdInfo = orgInfo;
    var dataLength = void 0;
    var leftLength = data.length;
    var state = false;
    // Red.log.debug(`data length = ${data.length}`);
    // Red.log.debug(data);
    while (leftLength) {
      if (pdInfo.dataCount === 0) {
        dataLength = data.copy(pdInfo.tmpbuffer, pdInfo.tmplength, data.length - leftLength, data.length);
        leftLength -= dataLength;

        // check waveform header '#xxxxx'
        pdInfo.tmplength += dataLength;

        pdInfo.header = pdInfo.tmpbuffer.slice(0, 20).toString();
        // Red.log.debug('data length = ' + data.length);
        // Red.log.debug('pdInfo.tmplength = ' + pdInfo.tmplength);
        // Red.log.debug('pdInfo.tmpbuffer = '  + pdInfo.tmpbuffer);
        // Red.log.debug('pdInfo.header = '  + pdInfo.header);
        // Red.log.debug('-------------------- check screen data header ----------------');

        if (pdInfo.header[0] === '#') {
          var num = Number(pdInfo.header[1]);
          var numstr = pdInfo.header.slice(2, Number(pdInfo.header[1]) + 2);

          pdInfo.headerlen = num + 2;
          // Red.log.debug('numstr=' + numstr);
          // block data length + NL
          pdInfo.dataCount = parseInt(numstr, 10);

          // Red.log.debug('pdInfo.dataCount = ' + pdInfo.dataCount);

          delete pdInfo.recData;
          pdInfo.recData = Buffer.allocUnsafe(pdInfo.dataCount);

          if (pdInfo.tmplength >= pdInfo.dataCount + pdInfo.headerlen) {
            pdInfo.pdHeadReady = false;

            dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.headerlen + pdInfo.dataCount);

            pdInfo.recCount = dataLength;
            // ////////////////
            if (dataLength === 0) {
              // Red.log.debug('33: copy faile = ' + dataLength);
              // pdInfo.recData = Buffer.allocUnsafe(pdInfo.dataCount);
              dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.headerlen + pdInfo.dataCount);
            }
            pdInfo.recCount = dataLength;

            // Red.log.debug('33: copy dataLength = ' + dataLength);
            // Red.log.debug(`33: pdInfo.recCount = ${pdInfo.recCount}`);
            // Red.log.debug(`33: pdInfo.headerlen = ${pdInfo.headerlen}`);
            // Red.log.debug(`33: pdInfo.headerlen +
            // pdInfo.dataCount = ${pdInfo.headerlen + pdInfo.dataCount}`);


            // savePDinfo(pdInfo.filepath, pdInfo);
            pdInfo.recCount = 0;
            state = true;
            if (pdInfo.tmplength > pdInfo.dataCount + pdInfo.headerlen) {
              // Red.log.debug('-------------------- continue ----------------');
              pdInfo.tmplength = shiftBufferData(pdInfo.tmpbuffer, pdInfo.headerlen, pdInfo.tmplength - pdInfo.headerlen);
              pdInfo.dataCount = 0;
              // continue;
            } else {
              pdInfo.tmpbuffer.fill(0);
              pdInfo.tmplength = 0;
              pdInfo.dataCount = 0;
              // return;
            }
          } else {
            dataLength = pdInfo.tmpbuffer.copy(pdInfo.recData, pdInfo.recCount, pdInfo.headerlen, pdInfo.tmplength);
            // Red.log.debug('4: coped dataLength = ' + dataLength);
            pdInfo.recCount = dataLength;
            pdInfo.tmpbuffer.fill(0);
            pdInfo.tmplength = 0;
            // return;
          }
        } else {
          pdInfo.tmplength = 0;
          pdInfo.header = '';
          myNode.status({ fill: 'red', shape: 'ring', text: 'header not recongnize' });
          pdInfo.pdHeadReady = false;
          return false;
        }
      } else {
        // Red.log.debug('-------------------- check screen data end ----------------');
        // Red.log.debug('pdInfo.recCount=' + pdInfo.recCount + ',data.length= ' + data.length);
        dataLength = data.copy(pdInfo.recData, pdInfo.recCount, data.length - leftLength, data.length);
        leftLength -= dataLength;
        // Red.log.debug('6: coped dataLength = ' + dataLength);
        // Red.log.debug('leftLength = ' + leftLength);

        pdInfo.recCount += dataLength;

        if (pdInfo.recCount === pdInfo.dataCount) {
          // Red.log.debug('saveScreen data 11');
          // savePDinfo(pdInfo.filepath, pdInfo);
          state = true;
          pdInfo.pdHeadReady = false;
          pdInfo.recCount = 0;
          pdInfo.dataCount = 0;
          pdInfo.header = '';
          // continue;
        }
      }
    }

    return state;
  };

  function pdDataNode(n) {
    // Create a RED node
    Red = RED;
    RED.nodes.createNode(this, n);
    this.pdDataType = n.pdDataType;
    this.pdHeadReady = false;
    this.pdHead = '';
    this.recData = Buffer.allocUnsafe(8192).fill(0, 0);
    this.recCount = 0;
    this.dataCount = 0;
    this.header = '';
    this.headerlen = 0;
    this.dataValidLen = 0;
    this.tmpbuffer = Buffer.allocUnsafe(8192).fill(0, 0);
    this.tmplength = 0;

    // copy "this" object in case we need it in context of callbacks of other functions.
    var node = this;
    myNode = this;

    // respond to inputs....
    this.on('input', (message) => {
      const msg = message;
      // RED.log.debug(`node.pdDataType = ${node.pdDataType}`);
      const { queryID, payload } = message;

      if (node.pdDataType === 'PD') {
        msg.payload = [];
        // Red.log.debug(' DataType = PD');
        node.pdm = [];

        myNode.status({});
        if (pdInformationDecode(node, payload) === true) {
          // node.send({
          //   payload: node.pdm,
          // });
          msg.payload = node.pdm;
          node.send(msg);
        }
      } else if (node.pdDataType === 'STRING') {
        msg.payload = payload.toString().split(String.fromCharCode(10))[0];
        node.send(msg);
      } else if (node.pdDataType === 'SCREEN') {
        msg.payload = [];
        // Red.log.debug(' DataType = PD');
        node.pdm = [];

        myNode.status({});
        if (screenDataDecode(node, payload) === true) {
          msg.payload = node.recData;
          node.send(msg);
        }
      }
    });
  }

  // Register the node by name. This must be called before overriding any of the
  // Node functions.
  RED.nodes.registerType('pd-data', pdDataNode);
};