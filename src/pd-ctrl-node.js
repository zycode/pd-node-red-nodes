module.exports = function (RED) {
  const cmdId = {
    SET_DSO_MODE: {
      val: 1,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = para;
        obj.para = '';
        obj.cmd = 1;
        obj.chSrc = 0;
        return obj;
      },
    },
    DAQ_CMD: {
      val: 2,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = 'DAQ';
        obj.para = para;
        obj.cmd = 2;
        obj.chSrc = 1;
        return obj;
      },
    },
    SET_MONITOR_CH: {
      val: 3,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');

        obj.dsoRole = 'PD';
        obj.para = pArray[1];
        obj.cmd = 3;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    SET_SENSOR_TYPE: {
      val: 4,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');

        obj.dsoRole = 'PD';
        obj.para = pArray[1];
        obj.cmd = 4;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    SET_PD_NOISE_RANGE: {
      val: 5,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');
        const fval = parseFloat(pArray[1]);

        obj.dsoRole = 'PD';
        obj.para = fval.toExponential();
        obj.cmd = 5;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    GET_PD_DATA: {
      val: 6,
      transFunc() {
        const obj = {};

        obj.dsoRole = 'PD';
        obj.para = '';
        obj.cmd = 6;
        obj.chSrc = 0;
        return obj;
      },
    },
    GET_DAQ_DATA: {
      val: 7,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = 'DAQ';
        obj.para = '';
        obj.cmd = 7;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
    DAQ_CMD_VSCALE: {
      val: 8,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');
        const fval = parseFloat(pArray[1]);

        obj.dsoRole = '';
        obj.para = fval.toExponential();
        obj.cmd = 8;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    SET_PD_SLOP: {
      val: 9,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');
        const fval = parseFloat(pArray[1]);

        obj.dsoRole = 'PD';
        obj.para = fval.toExponential();
        obj.cmd = 9;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    SET_PD_PULSE_WIDTH: {
      val: 10,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');
        const fval = parseFloat(pArray[1]);

        obj.dsoRole = 'PD';
        obj.para = fval.toExponential();
        obj.cmd = 10;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    // For test purpose
    SET_MUX_ENABLE: {
      val: 11,
      transFunc(para) {
        const obj = {};
        const pArray = para.split(':');

        obj.dsoRole = 'DAQ';
        obj.para = pArray[1];
        obj.cmd = 11;
        obj.chSrc = Number(pArray[0]) || 0;
        return obj;
      },
    },
    // For test purpose
    SET_MUX_CHANNEL: {
      val: 12,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = 'DAQ';
        obj.para = '';
        obj.cmd = 12;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
    SET_POWER_CYCLE: {
      val: 13,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = 'PD';
        if (para.toUpperCase().search('60HZ') !== -1) {
          obj.para = '1.6E-2';
        } else {
          obj.para = '2E-2';
        }
        obj.cmd = 13;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
    GET_PDM_VERSION: {
      val: 14,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = '';
        obj.para = '';
        obj.cmd = 14;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
    GET_CHANNEL_SETTING: {
      val: 15,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = '';
        obj.para = '';
        obj.cmd = 15;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
    SET_CHANNEL_ALL_PARA: {
      val: 16,
      transFunc(para) {
        const obj = {};
        const chPara = {};

        if (para.pulseWidth) {
          chPara.pulseWidth = parseFloat(para.pulseWidth).toExponential();
        }
        if (para.slope) {
          chPara.slope = parseFloat(para.slope).toExponential();
        }
        if (para.noiseRange) {
          chPara.noiseRange = parseFloat(para.noiseRange).toExponential();
        }
        if (para.state) {
          chPara.state = para.state;
        }
        if (para.sensorType) {
          chPara.sensorType = para.sensorType;
        }
        if (para.vscale) {
          chPara.vscale = para.vscale;
        }

        obj.dsoRole = '';
        obj.para = chPara;
        obj.cmd = 16;
        obj.chSrc = Number(para.chnum) || 0;
        return obj;
      },
    },
    GET_SCREEN_DATA: {
      val: 21,
      transFunc(para) {
        const obj = {};

        obj.dsoRole = 'DAQ';
        obj.para = '';
        obj.cmd = 21;
        obj.chSrc = Number(para) || 0;
        return obj;
      },
    },
  };

  function ctrlNode(n) {
    // Create a RED node
    RED.nodes.createNode(this, n);
    const node = this;

    this.dsoMode = n.dsoMode;
    this.cmd = n.cmd;
    this.para = n.para;
    this.chnum = n.chnum;
    this.queryID = n.queryID;
    // respond to inputs....
    this.on('input', (message) => {
      const msg = message;

      let pdCtrl = {};
      if (msg.cmd) {
        pdCtrl = cmdId[msg.cmd].transFunc(msg.para);
      } else {
        pdCtrl.dsoRole = node.dsoMode;
        pdCtrl.para = node.para;
        pdCtrl.cmd = Number(cmdId[node.cmd].val) || 0;
        pdCtrl.chSrc = Number(node.chnum) || 0;
      }
      if (node.queryID) {
        msg.queryID = node.queryID;
      }
      msg.payload = pdCtrl;
      node.send(msg);
    });
  }

  // Register the node by name. This must be called before overriding any of the
  // Node functions.
  RED.nodes.registerType('pd-ctrl', ctrlNode);
};
