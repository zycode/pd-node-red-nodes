var path = require('path');
var webpack = require('webpack');
var nodeExternals = require('webpack-node-externals');

module.exports ={
    entry: {
        "import-html": path.resolve( __dirname,'src/import-html.js'),
        "receive-data-node": path.resolve( __dirname,'src/receive-data-node.js'),
        "pd-ctrl-node": path.resolve( __dirname,'src/pd-ctrl-node.js')
    },
    output: {
        path: path.resolve( __dirname, 'nodes'),
        filename: '[name].js',
        library: 'driver',
        libraryTarget: 'umd'
    },
    target: 'node',
    externals: [nodeExternals()],
    module:{
        rules: [
            {
                test: /\.js$/,
                use:{
                    loader: 'babel-loader'
                }

            },
            {
                test: /\.json$/,
                use: 'json-loader'
            },
            {
                test: /\.(html)$/,
                use:[
                    {
                      loader: 'file-loader',
                      options: {
                        name: '[name].[ext]',
                        context: ''
                      }
                    }
                ]

            }            
        ]
    },
    devtool: 'source-map',
    stats:{
        colors: true
    }
}
